#uruchamianie skryptu z prawami administratora oraz bypass na uruchamiania blokade uruchamiania skryptu
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{ Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

#wypisanie nazwy komputera i wersji systemu operacyjnego
Get-CimInstance -ClassName Win32_ComputerSystem
Write-Host "`n Wersja systemu windows: "
(Get-WmiObject -class Win32_OperatingSystem).Caption
Write-Host "`n Nazwa uzytkonika: "
#Get-CimInstance -ClassName Win32_ComputerSystem | Select-Object Name,PrimaryOwnerName
(Get-CimInstance -ClassName Win32_ComputerSystem | select username).username
#[System.Security.Principal.WindowsIdentity]::GetCurrent().Name

#sprawdzanie wersji bios
$BootMode = bcdedit | Select-String "path.*efi"
if ($null -eq $BootMode) {
    # I think non-uefi is \Windows\System32\winload.exe
    $BootMode = "Legacy"
}else {
    # UEFI is: 
    #path                    \EFI\MICROSOFT\BOOT\BOOTMGFW.EFI
    #path                    \Windows\system32\winload.efi
    $BootMode = "UEFI"
}
Write-Host "`n Wersja BIOS na tym komputerze to: $BootMode"

# pierwsza wartosc okresla wersje TPM
$tpm = wmic /namespace:\\root\CIMV2\Security\MicrosoftTpm path Win32_Tpm get SpecVersion
Write-Host "`n Wersja TPM to pierwsza wartosc: " $tpm


#sprawdzenie fsecure poprawic tylko jak ?
function Check_Program_Installed( $programName ) {
    $wmi_check = (Get-WMIObject -Query "SELECT * FROM Win32_Process Where Name Like '%$programName%'")
    return $wmi_check;
}

Write-Host "`n Lista procecow powiazanych z Fsecure, moze byc pusta SPRAWDZ PANEL STEROWANIA "
Check_Program_Installed("F-Secure")
Get-ControlPanelItem -CanonicalName Microsoft.ProgramsAndFeatures | Show-ControlPanelItem

#sprawdzenie S/N
$SerialNumber = wmic bios get SerialNumber
Write-Host "`n S/N urzadzenia: " $SerialNumber

#sprawdzenie lokalnych adminow
Write-Host "Sprawdzanie lokalnych adminow"
lusrmgr.msc 
Get-LocalGroupMember -Group "Administratorzy" | Select-Object Name,PrincipalSource,ObjectClass


#aktualizowanie polityki
Write-Host "Aktualizowanie polityki z poziomu Admina: "
gpupdate /force

#rozpoczecie instalacji LAPS ktora jest automatyczna
$mypath = $MyInvocation.MyCommand.Path
$mypath1 = $mypath.Trim("skryptv2.ps1")
$mypath = $mypath1 + "LAPSx64.msi"
#Start-Process -FilePath $mypath
msiexec.exe /i  $mypath  ADDLOCAL=CSE ALLUSERS=1 /qn


#czekania na zakonczenie instalacji laps
Write-Host  "`n  LAPS zainstalowany wcisnij dowolny przycisk `n";
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

#aktualizowanie polityki po LAPS
gpupdate /force

#wypisanie dyskow
Write-Host "`n Istotne Dyski znajdujace sie na komputerze"
Get-CimInstance Win32_LogicalDisk | Select-Object DeviceID,DriveType,VolumeName,ProviderName

#sekcja bitlockera
$Error.Clear()
try {Get-ControlPanelItem -CanonicalName "Microsoft.BitLockerDriveEncryption" | Show-ControlPanelItem}
catch {"Panel nie uruchomi sie"}
if($Error) {
control /name Microsoft.BitLockerDriveEncryption
explorer shell:ControlPanelFolder
}

#czkeania na konfiguracje bitlockera i wydrukowanie klucza
Write-Host  "`n  Po rozpoczeciu szyfrowania wcisnij dowlny przycisk `n";
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

#sprawdzenie tpm w systemie
Write-Host "Jesli chcesz zakonczyc sprawdzenia wprowadz 0`n"
Write-Host "`n Sprawdzenia bitlocera"
while(($bitdisk =  Read-Host -Prompt "Wprowadz litere dysku z dwukropkiem") -ne "0"){
    switch ($bitdisk) {
        0 {"End"}
        Default { 
        manage-bde -protectors -get $bitdisk -type RecoveryPassword
        }
    }
   
}
Write-Host "Polecenie do przekopiwania w cmd `n"

Write-Host "manage-bde -protectors -adbackup c: -id"

#aktualizowanie polityki po Bitlockerze
gpupdate /force

#czekania na koniec
Write-Host  "`n  Aby zakonczyc wcisnij dowolny przycisk... `n";
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
