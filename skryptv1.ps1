#uruchamianie skryptu z prawami administratora
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

#aktualizowanie polityki
gpupdate /force

#wypisanie nazwy komputera i wersji systemu operacyjnego
Get-CimInstance -ClassName Win32_ComputerSystem
Write-Host "`n Wersja systemu windows: "
(Get-WmiObject -class Win32_OperatingSystem).Caption

#sprawdzanie wersji bios
$BootMode = bcdedit | Select-String "path.*efi"
if ($null -eq $BootMode) {
    # I think non-uefi is \Windows\System32\winload.exe
    $BootMode = "Legacy"
}else {
    # UEFI is: 
    #path                    \EFI\MICROSOFT\BOOT\BOOTMGFW.EFI
    #path                    \Windows\system32\winload.efi
    $BootMode = "UEFI"
}
Write-Host "`n Computer is running in $BootMode boot mode."

# pierwsza wartosc okresla wersje TPM
$tpm = wmic /namespace:\\root\CIMV2\Security\MicrosoftTpm path Win32_Tpm get SpecVersion
Write-Host "`n TPM version is: " $tpm

#sprawdzenie fsecure
function Check_Program_Installed( $programName ) {
    $wmi_check = (Get-WMIObject -Query "SELECT * FROM Win32_Process Where Name Like '%$programName%'").Length -gt 0
    return $wmi_check;
}

Write-Host "`n Czy F-Secure jest zainstalowany ? "
Check_Program_Installed("F-Secure")

#sprawdzenie S/N
$SerialNumber = wmic bios get SerialNumber
Write-Host "`n S/N urzadzenia: " $SerialNumber

#sprawdzenie lokalnych adminow
lusrmgr.msc 

#rozpoczecie instalacji LAPS podaj ścieżke 
Start-Process C:\Users\adi\Downloads\LAPSx64.msi

#czekania na zakonczenie instalacji laps
Write-Host  "`n  Po zainstalowaniu LAPS wcisnij dowolny przycisk `n";
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

#aktualizowanie polityki po LAPS
gpupdate /force

#wypisanie dyskow
#get-volume | Select-Object DriveLetter,FileSystemType,OperationalStatus,Size

#sekcja bitlockera
Get-ControlPanelItem -CanonicalName "Microsoft.BitLockerDriveEncryption" | Show-ControlPanelItem


#czkeania na konfiguracje bitlockera i wydrukowanie klucza
Write-Host  "`n  Po rozpoczeciu szyfrowania wcisnij dowlny przycisk `n";
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

#sprawdzenie tpm w systemie
Write-Host "Jesli chcesz zakonczyc sprawdzenia wprowadz 0`n"
while(($bitdisk =  Read-Host -Prompt "Wprowadz litere dysku z dwukropkiem") -ne "0"){
    switch ($bitdisk) {
        0 {"End"}
        Default { manage-bde -protectors -get $bitdisk -type RecoveryPassword}
    }
   
}
#wpisanie recznie bitlockera dla innych dyskow przy tpm 2.0
Write-Host "`n polecenie do przekopiowania"
Write-Host "`n manage-bde -protectors -adbackup c: -id "

#aktualizowanie polityki po Bitlockerze
gpupdate /force

#czekania na koniec
Write-Host  "`n  Aby zakonczyc wcisnij dowolny przycisk... `n";
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
